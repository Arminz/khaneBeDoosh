package ir.hotfix.khaneBeDoosh.utils;

import javax.servlet.http.HttpServletResponse;

public class Utils {
    public static String emptyToDefault(String value, String defaultValue){
        if (value == null || value.isEmpty())
            return defaultValue;
        return value;
    }

    public static String unknownPhone(String phone)
    {
        return phone.substring(0, 2) + "******" + phone.substring(phone.length() - 3, phone.length() - 1);
    }

    public static HttpServletResponse fixResponseHeaders(HttpServletResponse resp){
        resp.setContentType("application/json");
        resp.addHeader("Access-Control-Allow-Origin" , "*");
        resp.addHeader("Access-Control-Allow-Methods" , "GET, POST, PATCH, PUT, DELETE, OPTIONS");
        resp.addHeader("Access-Control-Allow-Headers" , "Origin, Content-Type, X-Auth-Token");
        resp.setCharacterEncoding("UTF-8");
        return resp;
    }
//    public static String malformedDealTypeToDefault(String value , String defaulValue){
//        if(!value.equals("خرید") && !value.equals("اجاره")){
//            return defaulValue;
//        }
//        return value;
//    }
}
