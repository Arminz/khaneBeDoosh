package ir.hotfix.khaneBeDoosh.server;

import com.google.gson.Gson;
import ir.hotfix.khaneBeDoosh.database.UserRepository;
import ir.hotfix.khaneBeDoosh.utils.AppConstants;
import ir.hotfix.khaneBeDoosh.model.Individual;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

import static ir.hotfix.khaneBeDoosh.utils.AppConstants.HOST_NAME;

/**
 * Created by armin on 2/16/18.
 * class for bank server
 */
public class BankServer {

    private static String urlEndpoint = HOST_NAME + "Bank/pay";

    private static String doPostIncreaseValue(Integer userId, int value) throws IOException {
        URL url = new URL(urlEndpoint);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setRequestProperty("Content-Language", "en-US");
        String json = String.format("{\"userId\":%s,\"value\":\"%s\"}", userId, value);
        connection.setRequestProperty("apiKey", AppConstants.API_KEY);
        connection.setRequestProperty("Content-Length", String.valueOf(json.getBytes().length));
        connection.setDoInput(true);
        connection.setDoOutput(true);
        connection.setUseCaches(false);

        OutputStream wr = connection.getOutputStream();
            wr.write(json.getBytes("UTF-8"));
        wr.flush();
        wr.close();

        InputStream is = connection.getInputStream();
        BufferedReader rd = new BufferedReader(new InputStreamReader(is));
        StringBuilder response = new StringBuilder();
        String line;
        while ((line = rd.readLine()) != null) {
            response.append(line);
            response.append('\r');
        }
        rd.close();
        connection.disconnect();
        return response.toString();

    }


    public static BankResult increaseValue(Integer userId, int value)
    {
        String result;
        try {
            result = doPostIncreaseValue(userId, value);
        } catch (IOException e) {
            e.printStackTrace();
            return new BankResult().setMessage("سرور ACM پاسخگو نمیباشد!").setResult("ERR");
        }
        Gson gson = new Gson();
        BankResult bankResult = gson.fromJson(result, BankResult.class);
        try{
            if (bankResult.getSuccess() != null && bankResult.getSuccess().equals("OK")) {
//                Individual.getInstance().increaseBalance(value);
                boolean increaseValueDBResult = UserRepository.increaseBalance(Individual.getInstance().getId(), value);
                if (!increaseValueDBResult)
                    return new BankResult().setMessage("مشکلی به وجود آمد.");
            }
        } catch (Exception e)
        {
            System.out.println("result : " + result);
        }
        return gson.fromJson(result, BankResult.class);

    }

    public static class BankResult{

        private String result;

        private String message;

        public String getSuccess() {
            return result;
        }

        public BankResult setResult(String result) {
            this.result = result;
            return this;
        }

        public String getMessage() {
            return message;
        }

        public BankResult setMessage(String message) {
            this.message = message;
            return this;
        }
    }

}
