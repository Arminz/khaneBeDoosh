package ir.hotfix.khaneBeDoosh.server;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import ir.hotfix.khaneBeDoosh.database.HouseRepository;
import ir.hotfix.khaneBeDoosh.model.House;
import ir.hotfix.khaneBeDoosh.model.RealState;
import org.omg.CORBA.TIMEOUT;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Array;
import java.sql.Timestamp;
import java.util.ArrayList;

import static ir.hotfix.khaneBeDoosh.utils.AppConstants.HOST_NAME;

public class HouseServer {
    private static String addr = HOST_NAME + "khaneBeDoosh/v2/house";

    public static House getSpecificHouse(String id){
        String res = getSpecificHouseJSON(id);
        Gson gson = new Gson();
        JsonObject json = gson.fromJson(res , JsonObject.class);
        if (json.get("data") == null)
        {

            return HouseRepository.getHouse(id);
        }
        return gson.fromJson(json.get("data") , House.class);
    }

    public static ArrayList<House> getHouses(Integer area , String buildingType , Integer dealType , Integer price){
        Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());
//        Timestamp expireTime = new Timestamp(System.currentTimeMillis());
        if(HouseRepository.getExpireTime() != null && currentTimestamp.before(HouseRepository.getExpireTime())){
            return HouseRepository.getHouses(area, buildingType, dealType, price);
        }
        else{
            HouseRepository.removeServerHouses();
            String res = getHousesJSON();
            Gson gson = new Gson();
            JsonObject json = gson.fromJson(res , JsonObject.class);
            HouseRepository.setExpireTime(new Timestamp(json.get("expireTime").getAsLong()));
            ArrayList<House> houses =  gson.fromJson(json.get("data") , new TypeToken<ArrayList<House>>(){}.getType());
//            int i = 0;
            for (House house : houses) {
//                i++;
//                if(i == 10)
//                    break;
                house.setRealStateId(RealState.getAcmInstance().getId());
                HouseRepository.insertHouse(house);
            }
            HouseRepository.setExpireTime(new Timestamp(json.get("expireTime").getAsLong()));
            System.out.println("salam!");
            return HouseRepository.getHouses(area, buildingType, dealType, price);
        }
        // check TimeStamp
        // if ok then getHouses(...)
        // else then removeServerHouse(...)  then request - insertHouse - getHouses
    }

    private static String getHousesJSON(){
        HttpURLConnection connection = null;
        try {
            URL url = new URL(addr);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type",
                    "application/json");

            connection.setRequestProperty("Content-Length",
                    "0");
            connection.setRequestProperty("Content-Language", "en-US");

            connection.setUseCaches(false);
            connection.setDoOutput(true);

            //Get Response
//            System.out.println(connection.getResponseCode());
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            StringBuilder response = new StringBuilder(); // or StringBuffer if Java version 5+
            String line;
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            rd.close();
            return response.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }

    }

    private static String getSpecificHouseJSON(String id){
        HttpURLConnection connection = null;
        try {
            URL url = new URL(addr + "/" + id);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type",
                    "application/json");

            connection.setRequestProperty("Content-Length",
                    "0");

            connection.setRequestProperty("Content-Language", "en-US");

            connection.setUseCaches(false);
            connection.setDoOutput(true);

            //Get Response
//            System.out.println(connection.getResponseCode());
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            StringBuilder response = new StringBuilder(); // or StringBuffer if Java version 5+
            String line;
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            rd.close();
            return response.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }


    }
}
