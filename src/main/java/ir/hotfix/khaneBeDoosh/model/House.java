package ir.hotfix.khaneBeDoosh.model;

/**
 * Created by armin on 2/16/18.
 * class for house
 */
public class House {

    private String id;

    private int area;

    private String buildingType;

    private String address;

    private String imageURL;

    private int dealType;

    private String phone;

    private String description;

    private String expireTime;

    private Price price;

    //Transient
    private boolean isBought;

    private Integer realStateId;

    public House(int area, String buildingType, String address, int dealType, String phone, String description, Price price, Integer realStateId , String id , String imageURL) {
        this.area = area;
        this.buildingType = buildingType;
        this.address = address;
        this.dealType = dealType;
        this.phone = phone;
        this.description = description;
        this.price = price;
        this.realStateId = realStateId;
        this.id = id;
        this.imageURL = imageURL;
    }

    public House() {
    }

    public String getId() {
        return id;
    }


    public void setId(String id) {
        this.id = id;
    }

    public int getArea() {
        return area;
    }

    public void setArea(int area) {
        this.area = area;
    }

    public String getBuildingType() {
        return buildingType;
    }

    public void setBuildingType(String buildingType) {
        this.buildingType = buildingType;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public int getDealType() {
        return dealType;
    }

    public void setDealType(int dealType) {
        this.dealType = dealType;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(String expireTime) {
        this.expireTime = expireTime;
    }

    public Price getPrice() {
        return price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }

    public boolean isBought() {
        return isBought;
    }

    public void setBought(boolean bought) {
        isBought = bought;
    }

    public Integer getRealStateId() {
        return realStateId;
    }

    public void setRealStateId(Integer realStateId) {
        this.realStateId = realStateId;
    }
}
