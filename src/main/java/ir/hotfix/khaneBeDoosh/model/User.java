package ir.hotfix.khaneBeDoosh.model;

import java.util.ArrayList;

/**
 * Created by armin on 2/16/18.
 * abstract class for users
 */
public abstract class User {

    private String name;

    private Integer id;


    public User(String name, Integer id) {
        this.name = name;
        this.id = id;
    }

    private User()
    {
    }

//    public ArrayList<String> getHouses()
//    {
//        return houses;
//    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
