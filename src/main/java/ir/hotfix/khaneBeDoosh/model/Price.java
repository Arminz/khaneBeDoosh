package ir.hotfix.khaneBeDoosh.model;

/**
 * Created by armin on 2/16/18.
 * class for price
 */
public class Price {

    private Integer basePrice;

    private Integer sellPrice;

    private Integer rentPrice;

    public Price(Integer basePrice, Integer sellPrice, Integer rentPrice) {
        this.basePrice = basePrice;
        this.sellPrice = sellPrice;
        this.rentPrice = rentPrice;
    }

    public Integer getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(Integer basePrice) {
        this.basePrice = basePrice;
    }

    public Integer getSellPrice() {
        return sellPrice;
    }

    public void setSellPrice(Integer sellPrice) {
        this.sellPrice = sellPrice;
    }

    public Integer getRentPrice() {
        return rentPrice;
    }

    public void setRentPrice(Integer rentPrice) {
        this.rentPrice = rentPrice;
    }
}
