package ir.hotfix.khaneBeDoosh.model;

import ir.hotfix.khaneBeDoosh.database.UserRepository;

/**
 * Created by armin on 2/16/18.
 * class for individual users
 */
public class Individual extends User {

    private int balance;

    private String phone;

    private String username;

    private String password;

    public Individual(String name, Integer id, int balance , String phone) {
        super(name, id);
        this.balance = balance;
        this.phone = phone;
    }

    public static Individual getInstance(){
//        if(instance == null)
//            instance = new Individual("بهنام همایون",123,2000 , "12345678900");
//        return instance;
        return UserRepository.getIndividualUser(1);
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
