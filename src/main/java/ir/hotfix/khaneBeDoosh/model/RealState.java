package ir.hotfix.khaneBeDoosh.model;

import ir.hotfix.khaneBeDoosh.database.UserRepository;

/**
 * Created by armin on 2/16/18.
 * class for RealState users
 */
public class RealState extends User {

    public static RealState getAcmInstance(){
        return UserRepository.getRealEstateUser(1);
    }

    public static RealState getSystemInstance(){
        return UserRepository.getRealEstateUser(2);
    }

    public RealState(String name, Integer id) {
        super(name, id);
    }
}
