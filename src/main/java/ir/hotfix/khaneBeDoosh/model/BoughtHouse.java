package ir.hotfix.khaneBeDoosh.model;

/**
 * Created by armin on 4/26/18.
 * class for bought models
 */
public class BoughtHouse {

    private int userId;

    private String  houseId;

    public int getUserId() {
        return userId;
    }

    public BoughtHouse setUserId(int userId) {
        this.userId = userId;
        return this;
    }

    public String  getHouseId() {
        return houseId;
    }

    public BoughtHouse setHouseId(String houseId) {
        this.houseId = houseId;
        return this;
    }
}
