package ir.hotfix.khaneBeDoosh.database;

import ir.hotfix.khaneBeDoosh.model.BoughtHouse;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by armin on 4/26/18.
 * {@link BoughtHouse}
 */
public class BoughtHouseRepository {

    public static boolean insertBoughtHouse(BoughtHouse boughtHouse) {
        String sqlQuery = "INSERT INTO boughtHouse (userid, houseid) VALUES (?, ?)";
        try {
            Connection connection = DatabaseManager.getConnection();
            PreparedStatement preparedStatement = DatabaseManager.getConnection().prepareStatement(sqlQuery);
            preparedStatement.setInt(1, boughtHouse.getUserId());
            preparedStatement.setString(2, boughtHouse.getHouseId());
            preparedStatement.executeUpdate();
            connection.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static BoughtHouse getBoughtHouse(int userId, String houseId){
        String sqlQuery = "SELECT * FROM boughtHouse WHERE userid=? AND houseId=?";
        try {
            Connection connection = DatabaseManager.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
            preparedStatement.setInt(1, userId);
            preparedStatement.setString(2, houseId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (!resultSet.next())
                return null;
            BoughtHouse boughtHouse = new BoughtHouse().setUserId(resultSet.getInt("userid")).setHouseId(resultSet.getString("houseid"));
            connection.close();
            return boughtHouse;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}
