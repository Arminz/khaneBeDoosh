package ir.hotfix.khaneBeDoosh.database;

import ir.hotfix.khaneBeDoosh.model.Individual;
import ir.hotfix.khaneBeDoosh.model.RealState;
import org.apache.commons.dbcp.BasicDataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DatabaseManager {

    private static BasicDataSource dataSource;

    private static BasicDataSource getDataSource(){
        if (dataSource == null) {
            dataSource = new BasicDataSource();
            dataSource.setDriverClassName("org.sqlite.JDBC");
            dataSource.setUrl("jdbc:sqlite:src/main/res/database.db");
        }
        return dataSource;
    }

    public static Connection getConnection() throws SQLException {
        return getDataSource().getConnection();
    }

    public static void createDatabaseIfNotExists() throws SQLException {
        Connection connection = getConnection();
//        connection.prepareStatement("CREATE DATABASE database.db").executeUpdate();
        connection.prepareStatement("CREATE TABLE IF NOT EXISTS house (\n" +
                "\tid char(255) PRIMARY KEY,\n" +
                "\trealEstateId char(255),\n" +
                "\tserver INTEGER,\n" +
                "\tarea INTEGER,\n" +
                "\tbuildingType char(255),\n" +
                "\taddress char(255),\n" +
                "\timageURL char(255),\n" +
                "\tdealType INTEGER,\n" +
                "\tphone char(255),\n" +
                "\tdescription char(255),\n" +
                "\tbasePrice INTEGER,\n" +
                "\tsellPrice INTEGER,\n" +
                "\trentPrice INTEGER\n" +
                "\t\n" +
                ")").executeUpdate();
        connection.prepareStatement("CREATE TABLE IF NOT EXISTS individualUser (\n" +
                "\tid INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
                "\tname char(255),\n" +
                "\tbalance INTEGER,\n" +
                "\tphone char(255),\n" +
                "\tusername char(255),\n" +
                "\tpassword char(255)\n" +
                "\t\n" +
                ")").executeUpdate();
        connection.prepareStatement("CREATE TABLE IF NOT EXISTS realEstate (\n" +
                "\tid INTEGER PRIMARY KEY,\n" +
                "\tname char(255)\n" +
                ")").executeUpdate();
        connection.prepareStatement("CREATE TABLE IF NOT EXISTS boughtHouse (\n" +
                "\tuserid INTEGER,\n" +
                "\thouseid char(255),\n" +
                "\tFOREIGN KEY(userid) REFERENCES individualUser(id) ON DELETE CASCADE,\n" +
                "\tFOREIGN KEY(houseid) REFERENCES house(id) ON DELETE NO ACTION\n" +
                ")").executeUpdate();
        UserRepository.initialize();
        connection.close();
    }
}