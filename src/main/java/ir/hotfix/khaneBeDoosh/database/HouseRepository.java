package ir.hotfix.khaneBeDoosh.database;

import ir.hotfix.khaneBeDoosh.model.House;
import ir.hotfix.khaneBeDoosh.model.Price;
import ir.hotfix.khaneBeDoosh.model.RealState;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created by armin on 4/16/18.
 *
 */
public class HouseRepository {

    private static Timestamp expireTime;

    public static Timestamp getExpireTime() {
        return expireTime;
    }

    public static void setExpireTime(Timestamp expireTime) {
        HouseRepository.expireTime = expireTime;
    }

    public static boolean insertHouse(House house)
    {
        String sqlQuery = "INSERT INTO house (id, area, buildingType, address, imageURL, dealType, phone, description, basePrice, sellPrice, rentPrice, realEstateId) " +
                    "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
        try {
            Connection conn = DatabaseManager.getConnection();
            PreparedStatement preparedStatement = conn.prepareStatement(sqlQuery);
            preparedStatement.setString(1, house.getId());
            preparedStatement.setInt(2, house.getArea());
            preparedStatement.setString(3, house.getBuildingType());
            preparedStatement.setString(4, house.getAddress());
            preparedStatement.setString(5, house.getImageURL());
            preparedStatement.setInt(6, house.getDealType());
            preparedStatement.setString(7, house.getPhone());
            preparedStatement.setString(8, house.getDescription());
            preparedStatement.setInt(9, house.getPrice().getBasePrice() == null ? 0 : house.getPrice().getBasePrice());
            preparedStatement.setInt(10, house.getPrice().getSellPrice() == null ? 0 : house.getPrice().getSellPrice());
            preparedStatement.setInt(11, house.getPrice().getRentPrice() == null ? 0 : house.getPrice().getRentPrice());
            preparedStatement.setInt(12, house.getRealStateId());
            preparedStatement.executeUpdate();
            preparedStatement.close();
            conn.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }

    }

    public static boolean removeServerHouses()
    {
        String sqlQuery = "DELETE FROM house WHERE realEstateId = ?";
        try {
            Connection conn = DatabaseManager.getConnection();
            PreparedStatement preparedStatement = conn.prepareStatement(sqlQuery);
            preparedStatement.setInt(1, RealState.getAcmInstance().getId());
            preparedStatement.executeUpdate();
            preparedStatement.close();
            conn.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static ArrayList<House> getHouses(Integer minArea, String buildingType, Integer dealType, Integer price){
        String sqlQuery = "SELECT * FROM house where buildingType = ? and dealType = ? and area > ? and ";
        sqlQuery = (dealType == 0) ? sqlQuery + "sellPrice" : sqlQuery + "rentPrice";
        sqlQuery += " < ?";
        try {
            Connection conn = DatabaseManager.getConnection();
            PreparedStatement preparedStatement = conn.prepareStatement(sqlQuery);
            preparedStatement.setString(1, buildingType);
            preparedStatement.setInt(2, dealType);
            preparedStatement.setInt(3, minArea);
            preparedStatement.setInt(4, price);
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<House> houses = new ArrayList<>();
            while (resultSet.next()){
                House house = new House();
                house.setId(resultSet.getString("id"));
                house.setRealStateId(resultSet.getInt("realEstateId"));
                house.setArea(resultSet.getInt("area"));
                house.setBuildingType(resultSet.getString("buildingType"));
                house.setAddress(resultSet.getString("address"));
                house.setImageURL(resultSet.getString("imageURL"));
                house.setDealType(resultSet.getInt("dealType"));
                house.setPhone(resultSet.getString("phone"));
                house.setDescription(resultSet.getString("description"));
                house.setPrice(new Price(resultSet.getInt("basePrice"), resultSet.getInt("sellPrice"), resultSet.getInt("rentPrice")));
                houses.add(house);
            }
            conn.close();
            preparedStatement.close();
            return houses;

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static House getHouse(String id) {
        String sqlQuery = "SELECT * FROM house WHERE id = ?";
        try {
            Connection conn = DatabaseManager.getConnection();
            PreparedStatement preparedStatement = conn.prepareStatement(sqlQuery);
            preparedStatement.setString(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (!resultSet.next()) {
                return null;
            }
            House house = new House(resultSet.getInt("area"), resultSet.getString("buildingType"), resultSet.getString("address"), resultSet.getInt("dealType"), resultSet.getString("phone"),
                    resultSet.getString("description"), new Price(resultSet.getInt("basePrice"), resultSet.getInt("sellPrice"), resultSet.getInt("rentPrice")), resultSet.getInt("realEstateId"),
                    resultSet.getString("id"), resultSet.getString("imageURL"));
            conn.close();
            preparedStatement.close();
            return house;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}