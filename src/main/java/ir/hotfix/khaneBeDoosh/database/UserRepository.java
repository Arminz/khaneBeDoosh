package ir.hotfix.khaneBeDoosh.database;

import ir.hotfix.khaneBeDoosh.model.Individual;
import ir.hotfix.khaneBeDoosh.model.RealState;
import ir.hotfix.khaneBeDoosh.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by armin on 4/21/18.
 * repository for @{User} class
 */
public class UserRepository {

    public static void initialize(){
        UserRepository.insertUser(new Individual("بهنام همایون", 1, 1000, "09231344123"));
        UserRepository.insertRealEstate(new RealState("acm", 1));
        UserRepository.insertRealEstate(new RealState("system", 2));
    }

    public static boolean insertUser(Individual individualUser, Integer id) {
        String sqlQuery = "INSERT INTO individualUser (name, balance, phone, username, password, id) SELECT ?, ?, ?, ?, ?, ? " +
                "WHERE NOT EXISTS (SELECT * FROM individualUser WHERE id = ?)";
        try {
            Connection conn = DatabaseManager.getConnection();
            PreparedStatement preparedStatement = conn.prepareStatement(sqlQuery);
            preparedStatement.setString(1, individualUser.getName());
            preparedStatement.setInt(2, individualUser.getBalance());
            preparedStatement.setString(3, individualUser.getPhone());
            preparedStatement.setString(4, individualUser.getUsername());
            preparedStatement.setString(5, individualUser.getPassword());
            if (id != null) {
                preparedStatement.setInt(6, id);
                preparedStatement.setInt(7, id);
            }
            else{
                preparedStatement.setInt(7, individualUser.getId());
            }
            preparedStatement.executeUpdate();
            preparedStatement.close();
            conn.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean insertUser(Individual individualUser) {
        return insertUser(individualUser, null);
    }


    public static Individual getIndividualUser(Integer id) {
        String sqlQuery = "SELECT * FROM individualUser WHERE id = ?";
        try {
            Connection conn = DatabaseManager.getConnection();
            PreparedStatement preparedStatement = conn.prepareStatement(sqlQuery);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (!resultSet.next()) {
                return null;
            }
            Individual individual = new Individual(resultSet.getString("name"), resultSet.getInt("id"), resultSet.getInt("balance"), resultSet.getString("phone"));
            conn.close();
            preparedStatement.close();
            return individual;

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static RealState getRealEstateUser(Integer id) {
        String sqlQuery = "SELECT * FROM realEstate WHERE id = ?";
        try {
            Connection conn = DatabaseManager.getConnection();
            PreparedStatement preparedStatement = conn.prepareStatement(sqlQuery);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (!resultSet.next()) {
                return null;
            }
            RealState realState = new RealState(resultSet.getString("name"), resultSet.getInt("id"));
            conn.close();
            preparedStatement.close();
            return realState;

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static boolean insertRealEstate(RealState realEstate, Integer id) {
        String sqlQuery = "INSERT INTO realEstate(name, id) SELECT ?, ? WHERE NOT EXISTS (SELECT * FROM realEstate WHERE id = ?)";
        try {
            Connection conn = DatabaseManager.getConnection();
            PreparedStatement preparedStatement = conn.prepareStatement(sqlQuery);
            preparedStatement.setString(1, realEstate.getName());
            if (id != null) {
                preparedStatement.setInt(2, id);
                preparedStatement.setInt(3, id);
            }
            else{
                preparedStatement.setInt(3, realEstate.getId());
            }
            preparedStatement.executeUpdate();
            conn.close();
            preparedStatement.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean insertRealEstate(RealState realEstate) {
        return insertRealEstate(realEstate, null);
    }

    public static boolean increaseBalance(int id, int deltaValue){
        String sqlQuery = "UPDATE individualUser SET balance=balance+? WHERE id=?";
        try {
            Connection connection = DatabaseManager.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
            preparedStatement.setInt(1, deltaValue);
            preparedStatement.setInt(2, id);
            preparedStatement.executeUpdate();
            connection.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }
}