package ir.hotfix.khaneBeDoosh.servlet;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import ir.hotfix.khaneBeDoosh.database.DatabaseManager;
import ir.hotfix.khaneBeDoosh.database.HouseRepository;
import ir.hotfix.khaneBeDoosh.model.House;
import ir.hotfix.khaneBeDoosh.model.RealState;
import ir.hotfix.khaneBeDoosh.server.HouseServer;
import ir.hotfix.khaneBeDoosh.model.Individual;
import ir.hotfix.khaneBeDoosh.model.Price;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.UUID;

import static ir.hotfix.khaneBeDoosh.utils.Utils.emptyToDefault;
import static ir.hotfix.khaneBeDoosh.utils.Utils.fixResponseHeaders;

@WebServlet("/house")
public class HouseServlet extends HttpServlet {

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        try {
            DatabaseManager.createDatabaseIfNotExists();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        fixResponseHeaders(response);
        Price price;
        int dealType = 0 , area = 0;
        request.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        JsonObject object = new JsonObject();
        try {
            if (request.getParameter("dealType").equals("خرید")) {
                price = new Price(0, Integer.parseInt(request.getParameter("sellPrice").trim()), 0);
            } else if(request.getParameter("dealType").equals("اجاره")){
                price = new Price(Integer.parseInt(request.getParameter("sellPrice").trim()), 0 , Integer.parseInt(request.getParameter("rentPrice").trim()));
                dealType = 1;
            } else{
                throw new NumberFormatException();
            }
            area = Integer.parseInt(request.getParameter("area"));
        } catch (NumberFormatException e){
            object.addProperty("msg" , "invalid");
            out.println(object.toString());
//            request.setAttribute("msg" , "اطلاعات وارد شده نادرست میباشد!");
//            request.getRequestDispatcher("index.jsp").forward(request , response);
            return;
        }
        String id = UUID.randomUUID().toString();
        String relativeAdr = "/static/no-pic.jpg";
        HouseRepository.insertHouse(new House(area , emptyToDefault(request.getParameter("buildingType"),"apartment").trim() , emptyToDefault(request.getParameter("address") , "").trim() , dealType ,
                emptyToDefault(request.getParameter("phone") , "").trim()  , emptyToDefault(request.getParameter("description") , "").trim() , price , RealState.getSystemInstance().getId() , id , relativeAdr));
        object.addProperty("msg" , "ok");
        out.println(object.toString());
//        request.setAttribute("msg" , "خانه مورد نظر با موفقیت ثبت شد!");
//        request.getRequestDispatcher("index.jsp").forward(request , response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        fixResponseHeaders(response);
//        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        JsonObject object = new JsonObject();
        request.setCharacterEncoding("UTF-8");
        ArrayList<House> houses;
        try{
            houses = HouseServer.getHouses(Integer.parseInt(emptyToDefault(request.getParameter("minArea"), "0")), emptyToDefault(request.getParameter("buildingType") , "").trim(), emptyToDefault(request.getParameter("dealType") , "").trim().equals("خرید") ? 0 : 1,
                    Integer.parseInt(emptyToDefault(request.getParameter("maxPrice"), String.valueOf(Integer.MAX_VALUE))));
            Gson gson = new Gson();
            String jsonArray = gson.toJson(houses);
            object.addProperty("data" , jsonArray);
            out.println(object.toString());

        }catch (NumberFormatException e){
            object.addProperty("msg" , "فیلد های مورد نظر باید درست وارد شوند! یکی از فیلد های قیمت یا متراژ نادرست میباشد!");
            out.println(object.toString());
//            request.setAttribute("msg" , "فیلد های مورد نظر باید درست وارد شوند! یکی از فیلد های قیمت یا متراژ نادرست میباشد!");
//            request.getRequestDispatcher("index.jsp").forward(request , response);
        }
    }
}