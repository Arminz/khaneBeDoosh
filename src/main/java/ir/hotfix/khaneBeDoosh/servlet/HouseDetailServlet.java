package ir.hotfix.khaneBeDoosh.servlet;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import ir.hotfix.khaneBeDoosh.database.BoughtHouseRepository;
import ir.hotfix.khaneBeDoosh.database.DatabaseManager;
import ir.hotfix.khaneBeDoosh.model.House;
import ir.hotfix.khaneBeDoosh.server.HouseServer;
import ir.hotfix.khaneBeDoosh.model.Individual;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import static ir.hotfix.khaneBeDoosh.utils.Utils.fixResponseHeaders;
import static ir.hotfix.khaneBeDoosh.utils.Utils.unknownPhone;

/**
 * Created by armin on 2/16/18.
 * servlet class for payment
 */
@WebServlet("/house/detail")
public class HouseDetailServlet extends HttpServlet {

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        try {
            DatabaseManager.createDatabaseIfNotExists();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        String houseId = (String) req.getAttribute("houseId");
        fixResponseHeaders(resp);
        String houseId = req.getParameter("houseId");
        House house = HouseServer.getSpecificHouse(houseId);
        Individual user = Individual.getInstance();
        PrintWriter out = resp.getWriter();


        if (house == null){
            resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }
        else {
            String accessPhone = house.getPhone();
            if (BoughtHouseRepository.getBoughtHouse(user.getId(), house.getId()) != null) {
                house.setBought(true);
            } else {
                house.setBought(false);
                accessPhone = unknownPhone(house.getPhone());
//                house.setPhone(unknownPhone(house.getPhone()));

            }
            JsonObject object = new JsonObject();
            Gson gson = new Gson();
            JsonObject jsonObject = gson.toJsonTree(house).getAsJsonObject();
            jsonObject.remove("phone");
            jsonObject.addProperty("phone", accessPhone);

            object.addProperty("data" , jsonObject.toString());
            out.println(object.toString());
        }


    }
}