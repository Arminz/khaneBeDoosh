package ir.hotfix.khaneBeDoosh.servlet;

import com.google.gson.JsonObject;
import ir.hotfix.khaneBeDoosh.database.BoughtHouseRepository;
import ir.hotfix.khaneBeDoosh.database.UserRepository;
import ir.hotfix.khaneBeDoosh.model.BoughtHouse;
import ir.hotfix.khaneBeDoosh.model.Individual;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

import static ir.hotfix.khaneBeDoosh.utils.Utils.fixResponseHeaders;

/**
 * Created by armin on 2/16/18.
 * servlet class for payment
 */
@WebServlet("/house/buy")
public class BuyServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        fixResponseHeaders(resp);
        String houseId = req.getParameter("houseId");
        Individual individual = Individual.getInstance();
        PrintWriter out = resp.getWriter();
        JsonObject object = new JsonObject();
        BoughtHouse boughtHouse = BoughtHouseRepository.getBoughtHouse(individual.getId(), houseId);
        if (boughtHouse != null)
        {
//            req.setAttribute("buy", "rich");
            object.addProperty("message" , "قبلا خریداری شده است.");
        }else {
            if (individual.getBalance() >= 1000)
            {
//                individual.buyHouse(houseId);
                boolean insertBoughtResult = BoughtHouseRepository.insertBoughtHouse(new BoughtHouse().setHouseId(houseId).setUserId(individual.getId()));
                if (insertBoughtResult) {
                    boolean increaseBalanceResult = UserRepository.increaseBalance(individual.getId(), -1000);
                    if (!increaseBalanceResult)
                        object.addProperty("message", "بدون تغییر در اعتبار خانه خریداری شد:|");
                    else
                        object.addProperty("message" , "خریداری شد.");
                }
                else
                {
                    resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                    object.addProperty("message", "مشکلی در پایگاه داده به وجود آمد.");
                }

//                req.setAttribute("buy", "rich");
            }
            else {
//                req.setAttribute("buy" , "poor");
                resp.setStatus(HttpServletResponse.SC_PAYMENT_REQUIRED);
                object.addProperty("message", "ااعتبار کافی نیست.");
            }
        }
        out.println(object.toString());
//        req.getRequestDispatcher("detail.jsp?id=" + req.getParameter("id")).forward(req, resp);
    }
}
