package ir.hotfix.khaneBeDoosh.servlet;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import ir.hotfix.khaneBeDoosh.database.DatabaseManager;
import ir.hotfix.khaneBeDoosh.model.Individual;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import static ir.hotfix.khaneBeDoosh.utils.Utils.fixResponseHeaders;

/**
 * Created by armin on 2/16/18.
 * servlet class for payment
 */
@WebServlet("/user")
public class UserServlet extends HttpServlet {

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        try {
            DatabaseManager.createDatabaseIfNotExists();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        House house = HouseServer.getSpecificHouse(req.getParameter("houseId"));
        fixResponseHeaders(resp);
        PrintWriter out = resp.getWriter();
        JsonObject object = new JsonObject();
        Gson gson = new Gson();
        object.addProperty("data" , gson.toJson(Individual.getInstance()));
        out.println(object.toString());
    }
}
