package ir.hotfix.khaneBeDoosh.servlet;

import com.google.gson.JsonObject;
import ir.hotfix.khaneBeDoosh.database.DatabaseManager;
import ir.hotfix.khaneBeDoosh.server.BankServer;
import ir.hotfix.khaneBeDoosh.model.Individual;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import static ir.hotfix.khaneBeDoosh.utils.Utils.fixResponseHeaders;

/**
 * Created by armin on 2/16/18.
 * servlet class for payment
 */
@WebServlet("/user/pay")
public class PayServlet extends HttpServlet {

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        try {
            DatabaseManager.createDatabaseIfNotExists();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        fixResponseHeaders(resp);
        int value;
        PrintWriter out = resp.getWriter();
        JsonObject object = new JsonObject();
        Individual individual = Individual.getInstance();
        try {
            value = Integer.parseInt(req.getParameter("balance"));
        } catch (NumberFormatException e){
            object.addProperty("msg" , "مقدار اعتبار افزایشی نادرست میباشد!");
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
//            req.setAttribute("msg", "مقدار اعتبار افزایشی نادرست میباشد!");
//            req.getRequestDispatcher("index.jsp").forward(req, resp);
            out.println(object.toString());
            return;
        }
        Integer userId = Individual.getInstance().getId();
        BankServer.BankResult bankResult = BankServer.increaseValue(userId, value);
        if(bankResult.getSuccess().equals("OK")) {
            object.addProperty("msg" , "اعتبار شما با موفقیت افزایش یافت!");
            object.addProperty("balance", individual.getBalance());
//            req.setAttribute("msg", "اعتبار شما با موفقیت افزایش یافت!");
        }
        else {
            object.addProperty("msg" , "سرور پاسخگو نمیباشد!");
            resp.setStatus(HttpServletResponse.SC_SERVICE_UNAVAILABLE);
//            req.setAttribute("msg", "مقدار اعتبار افزایشی نادرست میباشد!!");
        }
        out.println(object.toString());
//        req.getRequestDispatcher("index.jsp").forward(req, resp);
    }
}
