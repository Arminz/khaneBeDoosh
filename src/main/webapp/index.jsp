<%@ page import="ir.hotfix.khaneBeDoosh.model.Individual" %>
<%--
  Created by IntelliJ IDEA.
  User: armin
  Date: 2/16/18
  Time: 12:36 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page pageEncoding="UTF-8"%>
<html>
  <meta charset="UTF-8">
  <head>
    <title>خانه به دوش</title>
  </head>
  <body dir="rtl">
    <div>
        <p style="float: right;">
            نام کاربری :‌
            <%=
               Individual.getInstance().getName()
            %>
        </p>
        <p style="float: left;">
            اعتبار شما:
            <%=
                Individual.getInstance().getBalance()
            %>
        </p>
    </div>
    <br>
    <br>
    <br>
    <br>
    <br>
    <%
        request.setCharacterEncoding("UTF-8");
        if(request.getAttribute("msg") != null) {
    %>
    <h2 align="center">
        <%=
            request.getAttribute("msg")
        %>
    </h2>
    <%
        }
    %>
    <form action="house.jsp" method="get" accept-charset="UTF-8">
        <input type="text" name="minArea" placeholder="حداقل متراژ">
        <input type="text" name="buildingType" placeholder="نوع ملک">
        <br>
        <input type="text" name="dealType" placeholder="نوع قرارداد‌(خرید/اجاره)">
        <input type="text" name="maxPrice" placeholder="حداکثر قیمت">
        <input type="submit" value="جست و جو">
    </form>
    <form action="/add" method="post" accept-charset="UTF-8">
        <input type="text" name="buildingType" placeholder="نوع ساختمان(ویلایی/آپارتمان)">
        <input type="text" name="area" placeholder="متراژ">
        <br>
        <input type="text" name="dealType" placeholder="نوع قرارداد‌(خرید/اجاره)">
        <input type="text" name="price" placeholder="قیمت فروش/اجاره">
        <br>
        <input type="text" name="address" placeholder="آدرس">
        <input type="text" name="phone" placeholder="شماره تلفن">
        <br>
        <input type="text" name="description" placeholder="توضیحات">
        <input type="submit" value="اضافه کردن خانه ی جدید">
    </form>
    <form action="/user/pay" method="post" accept-charset="UTF-8">
        <input type="text" name="balance" placeholder="اعتبار">
        <input type="submit" value="افزایش اعتبار">
    </form>
  </body>
</html>
