<%@ page import="ir.hotfix.khaneBeDoosh.model.House" %>
<%@ page import="ir.hotfix.khaneBeDoosh.server.HouseServer" %>
<%@ page import="ir.hotfix.khaneBeDoosh.model.Individual" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="static ir.hotfix.khaneBeDoosh.utils.Utils.emptyToDefault" %>
<%--
  Created by IntelliJ IDEA.
  User: amk6610
  Date: 2/16/18
  Time: 2:26 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<html>
<head>
    <title>مشخصات خانه ها</title>
</head>
<body dir="rtl">
    <div>
        <p style="float: right;">
            نام کاربری :‌
            <%=
            Individual.getInstance().getName()
            %>
        </p>
        <p style="float: left;">
            اعتبار شما:
            <%=
            Individual.getInstance().getBalance()
            %>
        </p>
    </div>
    <br>
    <br>
    <br>
    <br>
    <br>
    <%
        request.setCharacterEncoding("UTF-8");
        ArrayList<House> houses = new ArrayList<House>();
        try{
            houses = HouseServer.getHouses(Integer.parseInt(emptyToDefault(request.getParameter("minArea"), "0")), request.getParameter("buildingType").trim(), request.getParameter("dealType").trim(),
                    Integer.parseInt(emptyToDefault(request.getParameter("maxPrice"), String.valueOf(Integer.MAX_VALUE))));
        }catch (NumberFormatException e){
            request.setAttribute("msg" , "فیلد های مورد نظر باید درست وارد شوند! یکی از فیلد های قیمت یا متراژ نادرست میباشد!");
            request.getRequestDispatcher("index.jsp").forward(request , response);
        }
    %>
    <table align="center" dir="rtl">
        <%
            for (House house : houses) {
        %>
        <tr>
            <td>
                <%
                    if (house.getPrice().getBasePrice() != null && house.getPrice().getBasePrice() != 0) {
                %>
                <p>
                    قیمت پایه:
                    <%=
                    house.getPrice().getBasePrice()
                    %>
                </p>
                <br>
                <%
                    }
                    if (house.getPrice().getRentPrice() != null && house.getPrice().getRentPrice() != 0) {
                %>
                <p>
                    مبلغ اجاره:
                    <%=
                    house.getPrice().getRentPrice()
                    %>
                </p>
                <br>
                <%
                    }
                    if (house.getPrice().getSellPrice() != null && house.getPrice().getSellPrice() != 0) {
                %>
                <p>
                    قیمت:
                    <%=
                    house.getPrice().getSellPrice()
                    %>
                </p>
                <br>
                <%
                    }
                %>
                <p>
                    متراژ:
                    <%=
                    house.getArea()
                    %>
                </p>
                <br>
                <p>
                    نوع:
                    <%
                        if (house.getDealType() == 1) {
                            out.print("رهن و اجاره");
                        } else
                            out.print("فروش");
                    %>
                </p>
                <br>
                <p>
                    لینک عکس:
                    <a href="<%= house.getImageURL() %>">
                        <%=
                        house.getImageURL()
                        %>
                    </a>
                </p>
                <br>
                <form action="/detail.jsp" method="get">
                    <input type="hidden" name="id" value="<%= house.getId()%>">
                    <input type="submit" value="اطلاعات بیشتر">
                </form>
            </td>
        </tr>
        <%
            }
        %>
    </table>
</body>
</html>
