<%-- Created by IntelliJ IDEA.
User: armin
Date: 2/16/18
Time: 2:25 PM
To change this template use File | Settings | File Templates.
--%>
<%@ page import="ir.hotfix.khaneBeDoosh.model.House" %>
<%@ page import="ir.hotfix.khaneBeDoosh.server.HouseServer" %>
<%@ page import="ir.hotfix.khaneBeDoosh.model.Individual" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>مشخصات خانه</title>
</head>
<body>
    <div>
        <p style="float: right;">
            نام کاربری :‌
            <%=
            Individual.getInstance().getName()
            %>
        </p>
        <p style="float: left;">
            اعتبار شما:
            <%=
            Individual.getInstance().getBalance()
            %>
        </p>
    </div>
    <br>
    <br>
    <br>
    <br>
    <br>
    <%
        if(request.getAttribute("msg") != null) {
            out.println(request.getAttribute("msg"));
        }
    %>
    <table align="center" dir="rtl">
        <%
            House house = HouseServer.getSpecificHouse(request.getParameter("id"));
            if(house == null){
                request.setAttribute("msg" , "خانه مورد نظر یافت نشد!");
                request.getRequestDispatcher("index.jsp").forward(request , response);
            } else{
        %>
        <tr>
            <td>نوع ساختمان:
            <%=
            house.getBuildingType()
            %>
            </td>
        </tr>
        <tr>
            <td>متراژ:
            <%=
            house.getArea()
            %>
            </td>
        </tr>
        <tr><td> نوع قرارداد:
            <%=
            house.getDealType()==0?"فروش":"رهن و اجاره"
            %>
            </td>
        </tr>
        <%
            if (house.getPrice().getBasePrice() != null){
        %>
        <tr><td> قیمت پایه:
            <%=
            house.getPrice().getBasePrice()
            %>
            </td>
        </tr>
        <%
            }
        %>
        <%
            if (house.getPrice().getRentPrice() != null){
        %>
        <tr><td> قیمت اجاره:
            <%=
            house.getPrice().getRentPrice()
            %>
            </td>
        </tr>
        <%
            }
        %>
        <%
            if (house.getPrice().getSellPrice() != null){
        %>
        <tr><td> قیمت فروش:
            <%=
            house.getPrice().getSellPrice()
            %>
            </td>
        </tr>
        <%
            }
        %>

        <tr><td> آدرس:
            <%=
            house.getAddress()
            %>
            </td>
        </tr>
        <tr><td>
            لینک عکس:
            <a href="<%= house.getImageURL() %>">
                <%=
                house.getImageURL()
                %>
            </a>
            </td>
        </tr>
        <tr><td> توضیحات:
            <%=
            house.getDescription()
            %>
            </td>
        </tr>
        <%
            if (request.getAttribute("buy") != null && request.getAttribute("buy").equals("rich"))
            {
        %>
        <tr><td> شماره تلفن:
            <%=
                house.getPhone()
            %>
            </td>
        </tr>
        <%
            }else if (request.getAttribute("buy") != null && request.getAttribute("buy").equals("poor")){
        %>
        <tr>
            <td>اعتبار شما برای دریافت شماره ی مالک/مشاور کافی نیست!
            </td>
        </tr>
        <%
            }else{
        %>
        <tr>
            <td>
                <form action="/house/1/buy" method="post">
                    <input type="hidden" name="houseId" value="<%= house.getId()%>">
                    <input type="hidden" name="id" value="<%= request.getParameter("id")%>">
                    <input type="submit" value="دریافت شماره ی مالک/مشاور">
                </form>
            </td>
        </tr>
        <%
                }
        %>
        <tr>
            <td>
                <form action="index.jsp" method="get">
                    <input type="submit" value="بازگشت به صفحه اصلی">
                </form>
            </td>
        </tr>
        <%
            }
        %>
    </table>

</body>
</html>
