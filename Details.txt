User
{
	name : String
	id : int
	Arraylist<int> houses
}

	Individual
	{
		phone : String
		balance : int
		username : String
		password : String
	}

	RealState
	{
		
	}

House
{
	id : String
	area : 	int
	buildingType : String
	address : String
	imageURL : String
	dealType : int
	phone : String
	description : String
	expireTime : String
	price : Price
}

Price
{
	sellPrice : int
	rentPrice : int
	basePrice : int
}

#Rest API
GET	/house/		--get all houses (Servlet)
POST /house/	--add a new house and forward to root (Servlet)
POST /user/pay	--increase the value of the user (Servlet)
GET /house/detail?houseId=id	--get details of a house (Servlet)
POST /house/buy?houseId=id  --buy phone of a house (Servlet)
GET /user               --get current user